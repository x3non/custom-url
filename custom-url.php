<?php
/**
 * Created by PhpStorm.
 * User: moman
 * Date: 10/26/2017
 * Time: 7:10 PM
 */

$reffErr="";
if($_SERVER["REQUEST_METHOD"]=="GET"){
    if(isset($_GET['reff']) && !empty("reff")){
        $reff= validate($_GET["reff"]);
        header("Location: http://account.orbitglobalearning.com/?ref=$reff");
        die();
    }
    else{
        echo "";
    }
}
function validate ($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>







<!DOCTYPE html>
<html lang="en">
<head>
    <title>Custom Link</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="url-css.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <!--<div class="col-md-4 col-sm-6 col-lg-4 col-md-offset-4 col-sm-offset-4">
            <h2>Reff Info</h2>
            <form >
                <div class="form-group">
                    <label>Reff ID</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input name="reff" id="email" placeholder="Refferer ID" class="form-control"  type="text" required>
                    </div>
                </div>
                <div class="form-group">
                    <div>
                        <button type="submit" class="btn btn-info">Continue</button>
                    </div>
                </div>
            </form>
        </div>-->

        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="profile-img" src="logo.jpg"
                     alt="">
                <h1 class="text-center login-title">Refferer Information</h1>
                <form class="form-signin" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="get">
                    <input type="text" minlength="3" class="form-control" placeholder="Refferer ID" name="reff" required autofocus>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Continue</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>

